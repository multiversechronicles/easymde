## EasyMDE

Drupal integration for [EasyMDE Markdown Editor](https://easymde.tk/):

> A drop-in JavaScript text area replacement for writing beautiful and
understandable Markdown. EasyMDE allows users who may be less experienced with
Markdown to use familiar toolbar buttons and shortcuts.

> In addition, the syntax is rendered while editing to clearly show the expected
result. Headings are larger, emphasized words are italicized, links are
underlined, etc.

> EasyMDE also features both built-in auto saving and spell checking. The
editor is entirely customizable, from theming to toolbar buttons and javascript
hooks.
