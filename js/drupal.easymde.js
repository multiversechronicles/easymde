/**
 * @file
 * SimpleMDE implementation of {@link Drupal.editors} API.
 */

(function (Drupal, EasyMDE) {

  'use strict';

  /**
   * @namespace
   */
  Drupal.editors.easymde = {

    /**
     * Editor attach callback.
     *
     * @param {HTMLElement} element
     *   The element to attach the editor to.
     * @param {string} format
     *   The text format for the editor.
     *
     * @return {bool}
     *   Whether the editor was successfully attached.
     */
    attach: function (element, format) {
      var textarea = document.getElementById(element.id);
      if (!textarea.classList.contains('easymde-processed')) {
        var settings = format.editorSettings;
        settings.element = textarea;
        settings.forceSync = true;
        settings.autoDownloadFontAwesome = false;
        settings.indentWithTabs = false;
        settings.lineNumbers = true;

        var editor = new EasyMDE(settings);

        // Support Insert module and potentially others that want to insert
        // content into the editor.
        editor.codemirror.getInputField().insertHtml = function(content) {
          editor.codemirror.operation(function() {
            editor.codemirror.getDoc().replaceSelection(content);
            editor.codemirror.execCommand("newlineAndIndent");
          });
          editor.codemirror.focus()
        }

        // In some cases insert will use the original textarea, so make sure to
        // also support it there.
        textarea.insertHtml = editor.codemirror.getInputField().insertHtml;

        textarea.easymde = editor;
        textarea.classList.add('easymde-processed');

        return !!editor;
      }
    },

    /**
     * Editor detach callback.
     *
     * @param {HTMLElement} element
     *   The element to detach the editor from.
     * @param {string} format
     *   The text format used for the editor.
     * @param {string} trigger
     *   The event trigger for the detach.
     *
     * @return {bool}
     *   Whether the editor was successfully detached.
     */
    detach: function (element, format, trigger) {
      // Temporary hack: Insert module's "Add Media" button triggers a detach
      // that removes the editor from the text area without it being re-added.
      // So disable detach for now.
      return false;

      var textarea = document.getElementById(element.id);
      if (textarea.classList.contains('easymde-processed')) {
        var editor = textarea.easymde;
        editor.toTextArea()
        textarea.easymde = null;
        textarea.classList.remove('easymde-processed');
        return true;
      }

      return false;
    },

    /**
     * Reacts on a change in the editor element.
     *
     * @param {HTMLElement} element
     *   The element where the change occurred.
     * @param {function} callback
     *   Callback called with the value of the editor.
     */
    onChange: function (element, callback) {
      callback();
    }
  };

})(Drupal, EasyMDE);
